import io.getquill._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}
object Quill extends App {
  val db = new CassandraAsyncContext(SnakeCase, "db")
  import db._
  case class User(name: String, age: Int)
  val q = quote {
    query[User].map(_.name)
  }
  val result = db.run(q)
  result.onComplete {
    case Success(value) => println(value)
    case Failure(e)     => e.printStackTrace()
  }
}
