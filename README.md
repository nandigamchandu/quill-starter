# Cassandra Starter
## Run Cassandra service if not already running
## Install cassandra 
#### on local machine
[Tutorial to install cassandra](http://cassandra.apache.org/doc/latest/getting_started/installing.html)
```bash
sudo service cassandra start
```
#### Alternatively, run cassandra in a docker container
[cassandra in a docker container](https://hub.docker.com/_/cassandra)
```bash
docker run --name some-cassandra -d -p 9042:9042 cassandra
```
#### To check cassandra is running or not
```bash
nodetool status
```
## Reference for Cassandra Query Language
[cql](http://cassandra.apache.org/doc/latest/cql/index.html)

### Create Keyspace if it doesn't already exist
```
CREATE KEYSPACE <keyspace-name>
    WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 3};
```

### Create this config file, named application.conf in resources folder
```
db.keyspace=<keyspace-name>
db.preparedStatementCacheSize=100
db.session.contactPoint=127.0.0.1
db.session.queryOptions.consistencyLevel=ONE
```
## Running

### Run the app

Make sure you're in the correct directory and run the `sbt` command

```bash
sbt run
```

[Reference for quill](https://getquill.io/#extending-quill-infix)

