name := "scala-cassandra"

version := "0.1"

scalaVersion := "2.12.8"
libraryDependencies ++= Seq("io.getquill" %% "quill-cassandra" % "3.4.1")
